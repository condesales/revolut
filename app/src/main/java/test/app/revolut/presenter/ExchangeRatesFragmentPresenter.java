package test.app.revolut.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Currency;

import javax.inject.Singleton;

import test.app.revolut.bus.CurrencyChangedEventBottom;
import test.app.revolut.bus.CurrencyChangedEventTop;
import test.app.revolut.bus.ValueChangedEventBottom;
import test.app.revolut.bus.ValueChangedEventTop;
import test.app.revolut.contract.ExchangeRatesFragmentContract;
import test.app.revolut.network.model.ExchangeRates;
import test.app.revolut.util.CustomPagerListener.PagerType;
import test.app.revolut.util.PriceUtil;


/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Singleton
public class ExchangeRatesFragmentPresenter implements ExchangeRatesFragmentContract.Presenter {

    private final ExchangeRatesFragmentContract.View mView;
    private final ExchangeRates mExchangeRates;
    private final int mType;
    private float mCurrentCompareRate;
    private boolean mHasFocus;

    public ExchangeRatesFragmentPresenter(ExchangeRatesFragmentContract.View view, Bundle args) {
        mView = view;
        mExchangeRates = (ExchangeRates) args.getSerializable(ExchangeRatesFragmentContract.EXCHANGE_RATES_KEY);
        mType = args.getInt(ExchangeRatesFragmentContract.TYPE);
    }

    @Override
    public void load() {
        Currency base = mExchangeRates.getBase();
        mView.showBaseCurrency(base);
        mView.showBaseAmount(PriceUtil.parseValue(base, 20f));
        mView.clearExchangeAmount();
    }

    @Override
    public void focusChanged(boolean hasFocus) {
        mHasFocus = hasFocus;
        mView.clearExchangeAmount();
        // clean the value events :)
        EventBus.getDefault().removeStickyEvent(ValueChangedEventTop.class);
        EventBus.getDefault().removeStickyEvent(ValueChangedEventBottom.class);
    }

    @Override
    public void textChanged(CharSequence text) {
        if (mHasFocus) {
            if (mType == PagerType.TOP) {
                EventBus.getDefault().postSticky(new ValueChangedEventTop(text));
            } else {
                EventBus.getDefault().postSticky(new ValueChangedEventBottom(text));
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onCompareChanged(CurrencyChangedEventTop event) {
        if (mType == PagerType.BOTTOM) {
            // update compare currency
            updateValues(event.getCurrency());
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onCompareChanged(CurrencyChangedEventBottom event) {
        if (mType == PagerType.TOP) {
            // update compare currency
            updateValues(event.getCurrency());
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onValueChanged(ValueChangedEventTop event) {
        if (mType == PagerType.BOTTOM) {
            updateText(event.getValue());
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onValueChanged(ValueChangedEventBottom event) {
        if (mType == PagerType.TOP) {
            updateText(event.getValue());
        }
    }

    private void updateText(CharSequence text) {
        String amount = calculateAmount(text);
        mView.showExchangeAmount(amount);
    }

    private String calculateAmount(CharSequence value) {
        // check if currency is the same
        if (mCurrentCompareRate == 1f) {
            return "";
        }
        String amountAsString = value.toString();
        float amount;
        try {
            amount = Float.parseFloat(amountAsString);
        } catch (NumberFormatException ex) {
            return "";
        }
        amount = Math.abs(amount);
        if (mType == PagerType.TOP) {
            // we need to invert its value
            amount = -amount;
        }
        return PriceUtil.parseValue(amount / mCurrentCompareRate);
    }

    private void updateValues(@NonNull Currency currencyCompare) {
        Currency base = mExchangeRates.getBase();
        if (currencyCompare.getCurrencyCode().equals(base.getCurrencyCode())) {
            mCurrentCompareRate = 1f;
            mView.hideComparison();
        } else {
            mCurrentCompareRate = mExchangeRates.getRates().get(currencyCompare);
            mView.showComparison(base, PriceUtil.parseRate(currencyCompare, mCurrentCompareRate));
        }
    }
}
