package test.app.revolut.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;
import test.app.revolut.Config;
import test.app.revolut.bus.CurrencyChangedEventBottom;
import test.app.revolut.bus.CurrencyChangedEventTop;
import test.app.revolut.bus.PageSelectionEvent;
import test.app.revolut.contract.ExchangeRatesContract;
import test.app.revolut.network.ExchangeRatesAPIClient;
import test.app.revolut.network.model.APIError;
import test.app.revolut.network.model.ExchangeRates;
import test.app.revolut.network.utils.CustomResponseCallback;
import test.app.revolut.util.CustomPagerListener.PagerType;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Singleton
public class ExchangeRatesActivityPresenter implements ExchangeRatesContract.Presenter, CustomResponseCallback<ExchangeRates> {

    private static final String BASE_KEY = "base";
    private static final String SYMBOLS_KEY = "symbols";
    private static final String DELIMITER = ",";
    private static final long POLL_RATE = 30000;//30 seconds
    private final ExchangeRatesContract.View mView;
    private final ExchangeRatesAPIClient mClient;
    private final Currency mBaseCurrency;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private ArrayList<ExchangeRates> mRatesAsList;
    private boolean mFirst = true;

    @Inject
    public ExchangeRatesActivityPresenter(ExchangeRatesContract.View view, ExchangeRatesAPIClient client) {
        mView = view;
        mClient = client;
        mBaseCurrency = Currency.getInstance(Locale.getDefault());
    }

    @Override
    public void getRates() {
        //avoid losing reference for a previous scheduled request
        stopGetRates();
        mTimer = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                performRequest();
            }
        };
        mTimer.scheduleAtFixedRate(mTimerTask, 0, POLL_RATE);
    }

    @Override
    public void stopGetRates() {
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask = null;
        }
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    public void onSuccess(ExchangeRates exchangeRates, Response response) {
        if (mView.isActive()) {
            // transform the result in an array with rates with different base
            mRatesAsList = new ArrayList<>();
            mRatesAsList.add(exchangeRates);
            for (Currency currency : exchangeRates.getRates().keySet()) {
                mRatesAsList.add(new ExchangeRates(currency, exchangeRates));
            }
            mView.loadRates(mRatesAsList);
            // when loading first time, we need to post that currency selected GBP
            if (mFirst) {
                EventBus.getDefault().postSticky(new CurrencyChangedEventBottom(mBaseCurrency));
                EventBus.getDefault().postSticky(new CurrencyChangedEventTop(mBaseCurrency));
                mFirst = false;
            }
        }
    }

    @Override
    public void onFailure(@NonNull APIError error) {
        if (mView.isActive())
            mView.showError(error);
    }

    @Subscribe
    public void pageSelectionChanged(PageSelectionEvent event) {
        //this will basically translate a selected position into a currency
        Currency currency = mRatesAsList.get(event.getPosition()).getBase();
        if (event.getType() == PagerType.BOTTOM) {
            EventBus.getDefault().postSticky(new CurrencyChangedEventBottom(currency));
        } else {
            EventBus.getDefault().postSticky(new CurrencyChangedEventTop(currency));
        }
    }

    private void performRequest() {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put(BASE_KEY, mBaseCurrency.getCurrencyCode());
        String symbols = TextUtils.join(DELIMITER, Config.SUPPORTED_CURRENCIES);
        queryMap.put(SYMBOLS_KEY, symbols);
        mClient.getExchangeRates(queryMap, this);
    }
}
