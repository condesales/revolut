package test.app.revolut;

import android.app.Application;
import android.content.Context;

import javax.inject.Inject;

import test.app.revolut.di.component.ApplicationComponent;
import test.app.revolut.di.component.DaggerApplicationComponent;
import test.app.revolut.di.module.ApplicationModule;
import test.app.revolut.di.module.NetworkModule;
import test.app.revolut.network.ExchangeRatesAPIClient;

import static test.app.revolut.Config.HOST;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class App extends Application {

    @Inject
    ExchangeRatesAPIClient mClient;
    private ApplicationComponent mApplicationComponent;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(HOST))
                .build();
        mApplicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
