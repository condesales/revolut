package test.app.revolut.di.component;

import dagger.Component;
import test.app.revolut.contract.ExchangeRatesContract;
import test.app.revolut.di.module.ExchangeRatesActivityModule;
import test.app.revolut.di.scopes.PerActivity;
import test.app.revolut.view.ExchangeRatesActivity;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ExchangeRatesActivityModule.class)
public interface ExchangeRatesActivityComponent {

    void inject(ExchangeRatesActivity activity);

    ExchangeRatesContract.Presenter getPresenter();
}
