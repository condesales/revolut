package test.app.revolut.di.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import test.app.revolut.App;
import test.app.revolut.di.module.ApplicationModule;
import test.app.revolut.di.module.NetworkModule;
import test.app.revolut.di.qualifiers.ApplicationContext;
import test.app.revolut.network.ExchangeRatesAPIClient;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(App application);

    @ApplicationContext
    Context getContext();

    Application getApplication();

    ExchangeRatesAPIClient getRateAPIClient();
}
