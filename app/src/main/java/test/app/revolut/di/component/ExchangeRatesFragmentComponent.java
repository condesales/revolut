package test.app.revolut.di.component;

import dagger.Component;
import test.app.revolut.contract.ExchangeRatesFragmentContract;
import test.app.revolut.di.module.ExchangeRatesFragmentModule;
import test.app.revolut.di.scopes.PerFragment;
import test.app.revolut.view.ExchangeRatesFragment;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@PerFragment
@Component(modules = ExchangeRatesFragmentModule.class)
public interface ExchangeRatesFragmentComponent {

    void inject(ExchangeRatesFragment fragment);

    ExchangeRatesFragmentContract.Presenter getPresenter();
}
