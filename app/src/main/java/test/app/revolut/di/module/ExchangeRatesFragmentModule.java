package test.app.revolut.di.module;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import dagger.Module;
import dagger.Provides;
import test.app.revolut.contract.ExchangeRatesFragmentContract;
import test.app.revolut.di.qualifiers.ActivityContext;
import test.app.revolut.presenter.ExchangeRatesFragmentPresenter;
import test.app.revolut.view.ExchangeRatesFragment;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Module
public class ExchangeRatesFragmentModule {

    private ExchangeRatesFragment mFragment;

    public ExchangeRatesFragmentModule(ExchangeRatesFragment fragment) {
        mFragment = fragment;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mFragment.getContext();
    }

    @Provides
    Activity provideActivity() {
        return mFragment.getActivity();
    }

    @Provides
    Bundle providesArgs() {
        return mFragment.getArguments();
    }

    @Provides
    ExchangeRatesFragmentContract.Presenter providesPresenter(Bundle args) {
        return new ExchangeRatesFragmentPresenter(mFragment, args);
    }
}
