package test.app.revolut.di.module;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import test.app.revolut.contract.ExchangeRatesContract;
import test.app.revolut.di.qualifiers.ActivityContext;
import test.app.revolut.network.ExchangeRatesAPIClient;
import test.app.revolut.presenter.ExchangeRatesActivityPresenter;
import test.app.revolut.view.ExchangeRatesActivity;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Module
public class ExchangeRatesActivityModule {

    private ExchangeRatesActivity mActivity;

    public ExchangeRatesActivityModule(ExchangeRatesActivity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    @Provides
    ExchangeRatesContract.Presenter providesPresenter(ExchangeRatesAPIClient client) {
        return new ExchangeRatesActivityPresenter(mActivity, client);
    }
}
