package test.app.revolut.di.qualifiers;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ExchangeRatesAPIInfo {
}
