package test.app.revolut.di.module;

import android.app.Application;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Currency;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import test.app.revolut.di.qualifiers.ExchangeRatesAPIInfo;
import test.app.revolut.network.ExchangeRatesAPI;
import test.app.revolut.network.utils.CurrencyTypeConverter;
import test.app.revolut.network.utils.CustomCallAdapterFactory;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Module
public class NetworkModule {
    private String mBaseUrl;

    public NetworkModule(@ExchangeRatesAPIInfo String mBaseUrl) {
        this.mBaseUrl = mBaseUrl;
    }

    @Provides
    @Singleton
    Cache provideHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        gsonBuilder.registerTypeAdapter(Currency.class, new CurrencyTypeConverter());
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(cache);
        return client.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(mBaseUrl)
                .addCallAdapterFactory(new CustomCallAdapterFactory(gson))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    ExchangeRatesAPI provideAPI(Retrofit retrofit) {
        return retrofit.create(ExchangeRatesAPI.class);
    }
}
