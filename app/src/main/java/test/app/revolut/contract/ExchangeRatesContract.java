package test.app.revolut.contract;

import java.util.ArrayList;

import test.app.revolut.contract.base.BasePresenter;
import test.app.revolut.contract.base.BaseView;
import test.app.revolut.network.model.APIError;
import test.app.revolut.network.model.ExchangeRates;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public interface ExchangeRatesContract {

    interface Presenter extends BasePresenter {

        void getRates();

        void stopGetRates();
    }

    interface View extends BaseView<Presenter> {

        void loadRates(ArrayList<ExchangeRates> rates);

        void showError(APIError error);
    }
}
