package test.app.revolut.contract;

import java.util.Currency;

import test.app.revolut.contract.base.BasePresenter;
import test.app.revolut.contract.base.BaseView;


/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public interface ExchangeRatesFragmentContract {

    String EXCHANGE_RATES_KEY = "exchange_rates";
    String TYPE = "type";

    interface Presenter extends BasePresenter {

        void load();

        void textChanged(CharSequence text);

        void focusChanged(boolean hasFocus);
    }

    interface View extends BaseView<Presenter> {

        void showBaseCurrency(Currency currency);

        void showBaseAmount(String baseAmount);

        void showExchangeAmount(CharSequence value);

        void clearExchangeAmount();

        void showComparison(Currency base, String compareRate);

        void hideComparison();
    }
}
