package test.app.revolut.network;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import test.app.revolut.network.model.ExchangeRates;
import test.app.revolut.network.utils.CustomNetworkCallInterface;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public interface ExchangeRatesAPI {

    @GET("latest")
    CustomNetworkCallInterface<ExchangeRates> getLatest(@QueryMap Map<String, String> options);
}
