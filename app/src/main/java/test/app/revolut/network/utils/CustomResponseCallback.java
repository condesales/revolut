package test.app.revolut.network.utils;

import android.support.annotation.NonNull;

import retrofit2.Response;
import test.app.revolut.network.model.APIError;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public interface CustomResponseCallback<T> {

    void onSuccess(T val, Response response);

    void onFailure(@NonNull APIError error);

}
