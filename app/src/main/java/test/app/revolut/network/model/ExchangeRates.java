package test.app.revolut.network.model;

import java.io.Serializable;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class ExchangeRates implements Serializable {

    private Currency base;
    private Map<Currency, Float> rates;

    /**
     * This will create a new ExchangeRates object for an specific currency, using another ExchangeRates object as base.
     * In order for this to work, the base needs to be in baseRate.rates.
     *
     * @param base              the new base to be considered
     * @param baseExchangeRates the rate where we gonna get all our info from in order to calculate the new rates for the base.
     */
    public ExchangeRates(Currency base, ExchangeRates baseExchangeRates) {
        this.base = base;
        this.rates = new HashMap<>();
        float scale = 1 / baseExchangeRates.getRates().get(base);
        this.rates.put(baseExchangeRates.base, scale);
        for (Currency currency : baseExchangeRates.getRates().keySet()) {
            if (!currency.getCurrencyCode().equals(base.getCurrencyCode())) {
                this.rates.put(currency, baseExchangeRates.getRates().get(currency) * scale);
            }
        }
    }

    public Currency getBase() {
        return base;
    }

    public Map<Currency, Float> getRates() {
        return rates;
    }
}
