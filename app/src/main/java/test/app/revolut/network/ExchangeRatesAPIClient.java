package test.app.revolut.network;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import test.app.revolut.network.model.ExchangeRates;
import test.app.revolut.network.utils.CustomNetworkCallInterface;
import test.app.revolut.network.utils.CustomResponseCallback;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
@Singleton
public class ExchangeRatesAPIClient {

    private final ExchangeRatesAPI exchangeRatesAPI;

    @Inject
    ExchangeRatesAPIClient(ExchangeRatesAPI api) {
        exchangeRatesAPI = api;
    }

    public CustomNetworkCallInterface<ExchangeRates> getExchangeRates(HashMap<String, String> queryMap, CustomResponseCallback<ExchangeRates> callback) {
        CustomNetworkCallInterface<ExchangeRates> latest = exchangeRatesAPI.getLatest(queryMap);
        latest.enqueue(callback);
        return latest;
    }

}
