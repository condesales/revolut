package test.app.revolut.network.utils;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.Executor;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.revolut.network.model.APIError;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
class CustomNetworkCall<T> implements CustomNetworkCallInterface<T>, Callback<T> {

    private final Call<T> call;
    private final Executor callbackExecutor;
    private final Gson gson;
    private CustomResponseCallback<T> mCallback;

    CustomNetworkCall(Call<T> call, Executor callbackExecutor, Gson gson) {
        this.call = call;
        this.callbackExecutor = callbackExecutor;
        this.gson = gson;
    }

    @Override
    public void cancel() {
        call.cancel();
    }

    @Override
    public void enqueue(final CustomResponseCallback<T> callback) {
        mCallback = callback;
        call.enqueue(this);
    }

    @Override
    public Response<T> execute() throws IOException {
        return call.execute();
    }

    @Override
    public CustomNetworkCallInterface<T> clone() {
        return new CustomNetworkCall<>(call.clone(), callbackExecutor, gson);
    }

    @Override
    public void onResponse(@NonNull final Call<T> call, @NonNull final Response<T> response) {
        if (call.isCanceled()) {
            return;
        }
        if (response.isSuccessful()) {
            if (mCallback != null) {
                callbackExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        mCallback.onSuccess(response.body(), response);
                    }
                });
            }
        } else {
            final APIError diceError = getError(response);
            if (mCallback != null) {
                callbackExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        mCallback.onFailure(diceError);
                    }
                });
            }
        }
    }

    @Override
    public void onFailure(@NonNull final Call<T> call, @NonNull final Throwable t) {
        if (call.isCanceled()) {
            return;
        }
        // same thing as in the onResponse ;)
        if (!(t instanceof NullPointerException)) {
            if (mCallback != null) {
                callbackExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        mCallback.onFailure(new APIError(t));
                    }
                });
            }
        }
    }

    @NonNull
    private APIError getError(Response<T> response) {
        APIError error;
        ResponseBody responseBody = response.errorBody();
        if (responseBody != null) {
            try {
                String string = responseBody.string();
                error = gson.fromJson(string, APIError.class);
                error.setStatus(response.code());
            } catch (Exception e) { // as generic as possible
                error = getDefaultError(response);
            }
        } else {
            error = getDefaultError(response);
        }
        return error;
    }

    /**
     * This error is created from response if for some reason we fail to parse the body sent from backend.
     *
     * @param response the original response
     * @return an error that uses the response status and message instead of body
     */
    @NonNull
    private APIError getDefaultError(Response<T> response) {
        APIError diceError = new APIError();
        if (response.message() != null) {
            diceError.setError(response.message());
        }
        diceError.setStatus(response.code());
        return diceError;
    }
}
