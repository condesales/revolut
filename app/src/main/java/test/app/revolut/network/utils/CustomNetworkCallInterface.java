package test.app.revolut.network.utils;

import java.io.IOException;

import retrofit2.Response;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public interface CustomNetworkCallInterface<T> {

    void cancel();

    void enqueue(CustomResponseCallback<T> callback);

    Response<T> execute() throws IOException;

    CustomNetworkCallInterface<T> clone();
}
