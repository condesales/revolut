package test.app.revolut.network.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Currency;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class CurrencyTypeConverter implements JsonSerializer<Currency>, JsonDeserializer<Currency> {

    @Override
    public JsonElement serialize(Currency src, Type srcType, JsonSerializationContext context) {
        return new JsonPrimitive(src.getCurrencyCode());
    }

    @Override
    public Currency deserialize(JsonElement json, Type type, JsonDeserializationContext context)
            throws JsonParseException {
        return Currency.getInstance(json.getAsString());
    }
}
