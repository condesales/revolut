package test.app.revolut.network.utils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class CustomCallAdapterFactory extends CallAdapter.Factory {

    private final Gson gson;
    private Executor callbackExecutor;

    public CustomCallAdapterFactory(Gson gson) {
        this.gson = gson;
        callbackExecutor = new CustomCallAdapterFactory.MainThreadExecutor();
    }

    @Override
    public CallAdapter<CustomNetworkCallInterface, ?> get(@NonNull Type returnType, @NonNull Annotation[] annotations, @NonNull Retrofit retrofit) {
        if (getRawType(returnType) != CustomNetworkCallInterface.class) {
            return null;
        }
        if (!(returnType instanceof ParameterizedType)) {
            throw new IllegalStateException("CustomNetworkCallInterface must have generic type (e.g., CustomNetworkCallInterface<ResponseBody>)");
        }
        final Type responseType = getParameterUpperBound(0, (ParameterizedType) returnType);
        return new CustomCallAdapter(responseType, callbackExecutor, gson);
    }

    private static class MainThreadExecutor implements Executor {
        private final Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable r) {
            handler.post(r);
        }
    }
}

