package test.app.revolut.network.utils;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.CallAdapter;

public class CustomCallAdapter implements CallAdapter<CustomNetworkCallInterface, CustomNetworkCall> {

    private final Type responseType;
    private final Executor callbackExecutor;
    private final Gson gson;

    CustomCallAdapter(Type responseType, Executor callbackExecutor, Gson gson) {
        this.responseType = responseType;
        this.callbackExecutor = callbackExecutor;
        this.gson = gson;
    }

    @Override
    public Type responseType() {
        return responseType;
    }

    @Override
    public CustomNetworkCall adapt(@NonNull Call<CustomNetworkCallInterface> call) {
        return new CustomNetworkCall<>(call, callbackExecutor, gson);
    }
}
