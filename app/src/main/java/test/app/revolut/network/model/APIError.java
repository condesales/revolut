package test.app.revolut.network.model;

import android.support.annotation.NonNull;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 02/10/2017.
 */
public class APIError implements Serializable {

    private static final String NETWORK_TYPE = "network_error";
    private static final String DEFAULT_TYPE = "generic_error";
    private static final String DEFAULT_DESCRIPTION = "something went wrong.";
    private String type = DEFAULT_TYPE;
    private String error = DEFAULT_DESCRIPTION;
    private int status = HttpURLConnection.HTTP_INTERNAL_ERROR;

    public APIError() {
        //will use default values
    }

    public APIError(@NonNull Throwable ex) {
        setError(ex);
        //check exception type
        if (isNetworkError(ex)) {
            // set more specific type
            type = NETWORK_TYPE;
            if (isTimeoutError(ex)) {
                //change for a more specific status
                status = HttpURLConnection.HTTP_CLIENT_TIMEOUT;
            }
        }
    }

    private boolean isNetworkError(Throwable ex) {
        return ex instanceof IOException;
    }

    private boolean isTimeoutError(Throwable ex) {
        return ex instanceof SocketTimeoutException || ex instanceof ConnectTimeoutException;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setError(@NonNull String errorMessage) {
        error = errorMessage;
    }

    @NonNull
    public String getError() {
        return error;
    }

    private void setError(@NonNull Throwable ex) {
        String localizedMessage = ex.getLocalizedMessage();
        String message = ex.getMessage();
        if (localizedMessage != null && !localizedMessage.isEmpty()) {
            error = localizedMessage;
        } else if (message != null && !message.isEmpty()) {
            error = message;
        } else {
            error = DEFAULT_DESCRIPTION;
        }
    }

}
