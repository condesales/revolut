package test.app.revolut.bus;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class ValueChangedEventBottom {

    private final CharSequence mValue;

    public ValueChangedEventBottom(CharSequence value) {
        mValue = value;
    }

    public CharSequence getValue() {
        return mValue;
    }
}
