package test.app.revolut.bus;

import test.app.revolut.util.CustomPagerListener.PagerType;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class PageSelectionEvent {

    @PagerType
    private final int mType;
    private final int mPosition;

    public PageSelectionEvent(@PagerType int type, int position) {
        mType = type;
        mPosition = position;
    }

    @PagerType
    public int getType() {
        return mType;
    }

    public int getPosition() {
        return mPosition;
    }
}
