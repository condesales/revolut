package test.app.revolut.bus;

import java.util.Currency;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class CurrencyChangedEventBottom {

    private final Currency mCurrency;

    public CurrencyChangedEventBottom(Currency currency) {
        mCurrency = currency;
    }

    public Currency getCurrency() {
        return mCurrency;
    }
}
