package test.app.revolut.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 13/10/2017.
 */
public class PriceUtil {

    private static final DecimalFormat numberFormat = new DecimalFormat("+#.##;-#.##");
    private static final NumberFormat currencyFormat = DecimalFormat.getCurrencyInstance();

    public static String parseValue(float value) {
        return numberFormat.format(value);
    }

    public static String parseValue(Currency currency, float value) {
        currencyFormat.setCurrency(currency);
        currencyFormat.setRoundingMode(RoundingMode.UNNECESSARY);
        // Check if we have places on the value
        if ((value % 1) == 0) {
            currencyFormat.setMaximumFractionDigits(0);
        } else {
            currencyFormat.setMinimumFractionDigits(currency.getDefaultFractionDigits());
        }
        try {
            return currencyFormat.format(value);
        } catch (ArithmeticException e) {
            currencyFormat.setRoundingMode(RoundingMode.HALF_EVEN);
            return currencyFormat.format(value);
        }
    }

    public static String parseRate(Currency currency, float value) {
        currencyFormat.setCurrency(currency);
        currencyFormat.setRoundingMode(RoundingMode.UNNECESSARY);
        currencyFormat.setMinimumFractionDigits(4);
        try {
            return currencyFormat.format(value);
        } catch (ArithmeticException e) {
            currencyFormat.setRoundingMode(RoundingMode.HALF_EVEN);
            return currencyFormat.format(value);
        }
    }
}
