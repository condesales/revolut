package test.app.revolut.util;

import android.support.annotation.IntDef;
import android.support.v4.view.ViewPager;

import org.greenrobot.eventbus.EventBus;

import java.lang.annotation.Retention;

import test.app.revolut.bus.PageSelectionEvent;

import static java.lang.annotation.RetentionPolicy.SOURCE;
import static test.app.revolut.util.CustomPagerListener.PagerType.BOTTOM;
import static test.app.revolut.util.CustomPagerListener.PagerType.TOP;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class CustomPagerListener implements ViewPager.OnPageChangeListener {

    @PagerType
    private final int mType;

    public CustomPagerListener(@PagerType int type) {
        mType = type;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        EventBus.getDefault().post(new PageSelectionEvent(mType, position));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Retention(SOURCE)
    @IntDef({BOTTOM, TOP})
    public @interface PagerType {
        int BOTTOM = 0;
        int TOP = 1;
    }

}
