package test.app.revolut.view;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.app.revolut.App;
import test.app.revolut.R;
import test.app.revolut.contract.ExchangeRatesContract;
import test.app.revolut.di.component.DaggerExchangeRatesActivityComponent;
import test.app.revolut.di.component.ExchangeRatesActivityComponent;
import test.app.revolut.di.module.ExchangeRatesActivityModule;
import test.app.revolut.network.model.APIError;
import test.app.revolut.network.model.ExchangeRates;
import test.app.revolut.util.CustomPagerListener;

import static test.app.revolut.util.CustomPagerListener.PagerType.BOTTOM;
import static test.app.revolut.util.CustomPagerListener.PagerType.TOP;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class ExchangeRatesActivity extends AppCompatActivity implements ExchangeRatesContract.View {

    @Inject
    ExchangeRatesContract.Presenter mPresenter;
    @BindView(R.id.exchange_rate_top)
    ViewPager mViewPagerTop;
    @BindView(R.id.exchange_rate_bottom)
    ViewPager mViewPagerBottom;
    private ExchangeRatesActivityComponent mComponent;
    private ExchangeRatesAdapter mAdapterBottom;
    private ExchangeRatesAdapter mAdapterTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhange_rates);
        ButterKnife.bind(this);
        getComponent().inject(this);
        setup();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getRates();
        EventBus.getDefault().register(mPresenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.stopGetRates();
        EventBus.getDefault().unregister(mPresenter);
    }

    @Override
    public void loadRates(ArrayList<ExchangeRates> rates) {
        mAdapterTop.load(rates);
        mAdapterBottom.load(rates);
    }

    @Override
    public void showError(APIError error) {
        Toast.makeText(this, error.getError(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return !isFinishing();
    }

    private void setup() {
        mAdapterTop = new ExchangeRatesAdapter(getSupportFragmentManager(), TOP);
        mViewPagerTop.setAdapter(mAdapterTop);
        mViewPagerTop.addOnPageChangeListener(new CustomPagerListener(TOP));
        mAdapterBottom = new ExchangeRatesAdapter(getSupportFragmentManager(), BOTTOM);
        mViewPagerBottom.setAdapter(mAdapterBottom);
        mViewPagerBottom.addOnPageChangeListener(new CustomPagerListener(BOTTOM));
    }

    private ExchangeRatesActivityComponent getComponent() {
        if (mComponent == null) {
            mComponent = DaggerExchangeRatesActivityComponent.builder()
                    .exchangeRatesActivityModule(new ExchangeRatesActivityModule(this))
                    .applicationComponent(App.get(this).getApplicationComponent())
                    .build();
        }
        return mComponent;
    }
}
