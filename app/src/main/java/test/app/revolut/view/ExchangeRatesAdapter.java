package test.app.revolut.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import test.app.revolut.network.model.ExchangeRates;

import static test.app.revolut.util.CustomPagerListener.PagerType;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class ExchangeRatesAdapter extends FragmentPagerAdapter {

    private final int mType;
    private ArrayList<ExchangeRates> mRates = new ArrayList<>();

    ExchangeRatesAdapter(FragmentManager fm, @PagerType int type) {
        super(fm);
        mType = type;
    }

    void load(ArrayList<ExchangeRates> rates) {
        mRates = rates;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return ExchangeRatesFragment.newInstance(mType, mRates.get(position));
    }

    @Override
    public int getCount() {
        return mRates.size();
    }
}
