package test.app.revolut.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.Currency;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import test.app.revolut.R;
import test.app.revolut.contract.ExchangeRatesFragmentContract;
import test.app.revolut.di.component.DaggerExchangeRatesFragmentComponent;
import test.app.revolut.di.component.ExchangeRatesFragmentComponent;
import test.app.revolut.di.module.ExchangeRatesFragmentModule;
import test.app.revolut.network.model.ExchangeRates;
import test.app.revolut.util.CustomPagerListener.PagerType;

import static test.app.revolut.contract.ExchangeRatesFragmentContract.TYPE;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class ExchangeRatesFragment extends Fragment implements ExchangeRatesFragmentContract.View {

    private static final String EXCHANGE_RATES_KEY = "exchange_rates";
    @Inject
    ExchangeRatesFragmentContract.Presenter mPresenter;
    @BindView(R.id.exchange_rate_amount)
    EditText mAmount;
    @BindView(R.id.exchange_rate_currency)
    TextView mCurrency;
    @BindView(R.id.exchange_rate_you_have)
    TextView mYouHave;
    @BindView(R.id.exchange_rate_comparison)
    TextView mComparison;
    private Unbinder unbinder;
    private ExchangeRatesFragmentComponent mComponent;

    public static ExchangeRatesFragment newInstance(@PagerType int type, ExchangeRates rates) {
        Bundle args = new Bundle();
        args.putSerializable(EXCHANGE_RATES_KEY, rates);
        args.putInt(TYPE, type);
        ExchangeRatesFragment fragment = new ExchangeRatesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exchange_rate, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.load();
        EventBus.getDefault().register(mPresenter);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(mPresenter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showBaseCurrency(Currency currency) {
        mCurrency.setText(currency.getCurrencyCode());
    }

    @Override
    public void showBaseAmount(String baseAmount) {
        mYouHave.setText(getString(R.string.you_have, baseAmount));
    }

    @Override
    public void showExchangeAmount(CharSequence value) {
        mAmount.setText(value);
    }

    @Override
    public void clearExchangeAmount() {
        mAmount.setText("");
    }

    @Override
    public void showComparison(Currency base, String compareRate) {
        mComparison.setVisibility(View.VISIBLE);
        mComparison.setText(getString(
                R.string.rate_comparison,
                base.getSymbol(),
                compareRate));
    }

    @Override
    public void hideComparison() {
        mComparison.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    private ExchangeRatesFragmentComponent getComponent() {
        if (mComponent == null) {
            mComponent = DaggerExchangeRatesFragmentComponent.builder()
                    .exchangeRatesFragmentModule(new ExchangeRatesFragmentModule(this))
                    .build();
        }
        return mComponent;
    }

    @OnFocusChange(R.id.exchange_rate_amount)
    public void onFocusChanged(boolean hasFocus) {
        mPresenter.focusChanged(hasFocus);
    }

    @OnTextChanged(R.id.exchange_rate_amount)
    public void onTextChanged(CharSequence text) {
        mPresenter.textChanged(text);
    }
}
