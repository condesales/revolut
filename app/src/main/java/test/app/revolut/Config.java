package test.app.revolut;

/**
 * <description>
 * <p/>
 * Copyright (c) 2017. All rights reserved.
 *
 * @author Felipe Conde (condesales@gmail.com)
 *         On 12/10/2017.
 */
public class Config {

    static final String HOST = "https://api.fixer.io//";
    private static final String GBP_SYMBOL = "GBP";
    private static final String EUR_SYMBOL = "EUR";
    private static final String USD_SYMBOL = "USD";
    public static final String[] SUPPORTED_CURRENCIES = new String[]{GBP_SYMBOL, EUR_SYMBOL, USD_SYMBOL};
}
